#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
SPDX-FileCopyrightText: 2014 Burkhard Lück <lueck@hube-lueck.de>
SPDX-FileCopyrightText: 2020 Andrius Štikonas <аndrius@stikonas.eu>

SPDX-License-Identifier: MIT
"""

import sys, os, glob, codecs
import polib

if len(sys.argv) != 6:
  print('\nUsage: python %s path/to/xmlfile path/to/l10ndir/ l10nmodulename pofilename.po /path/to/potfile.pot' %os.path.basename(sys.argv[0]))
else:
  xmlfilepath, xmlfilename, l10ndirpath, l10nmodulename, pofilename, potfile = sys.argv[1], sys.argv[1].split("/")[-1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]

  xmllines = codecs.open(xmlfilepath, "rb", "utf8").readlines()
  xmlwithtranslation = ''
  xmlpofilelist = glob.glob("%s/*/messages/%s/%s" %(l10ndirpath, l10nmodulename, pofilename))

  pot = polib.pofile(potfile)
  tags = set()
  for entry in pot:
    tags.add(entry.msgctxt)
  tags = list(tags)

  begintags = ["<" + tag + ">" for tag in tags]
  endtags = ["</" + tag + ">" for tag in tags]
  beginlangtags = ["<" + tag + " xml:lang=" for tag in tags]

  pofiledict = {}
  if not l10ndirpath.endswith("/"):
    l10ndirpath += "/"
  for pofile in xmlpofilelist:
    langcode = pofile.split(l10ndirpath)[1].split("/")[0]
    if langcode != "x-test":
      po = polib.pofile(pofile)
      pofiledict[langcode] = po
  for line in xmllines:
    presenttags = list(tag in line for tag in begintags)
    presenttagindices = list(filter(lambda i: presenttags[i], range(len(presenttags))))
    assert(len(presenttagindices) <= 1), "Too many tags in a single line"

    if any(presenttags):
      tagindex = presenttagindices[0]
      tag = tags[tagindex]
      begintag = begintags[tagindex]
      endtag = endtags[tagindex]
      beginlangtag = beginlangtags[tagindex]

      indentwidth = line.split(begintag)[0]
      xmlwithtranslation += line
      msgid = line.split(begintag)[1].split(endtag)[0]
      for lang, po in sorted(pofiledict.items()):
        msgstr = ''
        for entry in pofiledict[lang].translated_entries():
          if entry.msgid == msgid and entry.msgctxt == tag:
            msgstr = entry.msgstr
            break
        if msgstr != '':
          transline = '%s%s"%s">%s%s\n' %(indentwidth, beginlangtag, lang.replace('-', '_'), msgstr, endtag)
          xmlwithtranslation += transline
    elif any(tag in line for tag in beginlangtags):
      pass
    else:
      xmlwithtranslation += line

  modifiedxml = codecs.open("%s" %xmlfilepath, "w", "utf8")
  modifiedxml.write(xmlwithtranslation)
  modifiedxml.close()
