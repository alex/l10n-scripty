# The syntax of the file is easy
# Syntax:
#      entry dir_name scm_path
# Where
#      scm_path is
#        a kde git repo identifier followed by path_to_folder
#      dir_name is name of the entry e.g. konversation

# frameworks
entry kded6 kded docs/kded6
entry checkXML6 kdoctools docs/checkXML6
entry kf6options kdoctools docs/kf6options
entry meinproc6 kdoctools docs/meinproc6
entry qt6options kdoctools docs/qt6options
entry kioworker6 kio docs/kioworker6
entry kbuildsycoca6 kservice docs/kbuildsycoca6
entry kpackagetool kpackage docs/kpackagetool
entry kwallet-query kwallet docs/kwallet-query

# plasma
entry kcontrol plasma-desktop doc/kcontrol
entry kcontrol/kgamma kgamma doc
entry kfontview plasma-desktop doc/kfontview
entry knetattach plasma-desktop doc/knetattach
entry plasma-desktop plasma-desktop doc/plasma-desktop
entry kdesu kde-cli-tools doc/kdesu
entry kcontrol6 kde-cli-tools doc/kcontrol6
entry kinfocenter kinfocenter doc
entry kmenuedit kmenuedit doc
entry kcontrol/bluedevil bluedevil doc
entry kcontrol/desktop kwin doc/desktop
entry kcontrol/kwindecoration kwin doc/kwindecoration
entry kcontrol/kwinscreenedges kwin doc/kwinscreenedges
entry kcontrol/kwintabbox kwin doc/kwintabbox
entry kcontrol/kwintouchscreen kwin doc/kwintouchscreen
entry kcontrol/kwinvirtualkeyboard kwin doc/kwinvirtualkeyboard
entry kcontrol/windowbehaviour kwin doc/windowbehaviour
entry kcontrol/windowspecific kwin doc/windowspecific
entry kcontrol/kwineffects kwin doc/kwineffects
entry klipper plasma-workspace doc/klipper
entry kcontrol plasma-workspace doc/kcontrol
entry kcontrol/wacomtablet wacomtablet doc/user
##
##entry PolicyKit-kde plasma-workspace doc/PolicyKit-kde
##obsolete here, docs have to be moved away, see https://bugs.kde.org/show_bug.cgi?id=334400
##
entry kcontrol/plasma-pa plasma-pa doc/kcontrol/plasma-pa
entry systemsettings systemsettings doc
entry plasma-sdk_engineexplorer plasma-sdk engineexplorer
entry plasma-sdk_plasmoidviewer plasma-sdk plasmoidviewer

# applications
entry dolphin dolphin doc
entry kate kate doc/kate
entry kwrite kate doc/kwrite
entry katepart kate doc/katepart
entry kfind kfind doc
entry khelpcenter khelpcenter doc/khelpcenter
entry fundamentals khelpcenter doc/fundamentals
entry onlinehelp khelpcenter doc/onlinehelp
entry glossary khelpcenter doc/glossary
entry keditbookmarks keditbookmarks doc
entry konqueror konqueror doc/konqueror
entry kcontrol konqueror doc/kcontrol

entry konsole konsole doc/manual
entry partitionmanager partitionmanager doc

# kdeaccessibility
entry kmag kmag doc
entry kmousetool kmousetool doc
entry kmouth kmouth doc
entry kontrast kontrast doc

# kdeadmin
entry kcontrol/kcron kcron doc/kcontrol
entry ksystemlog ksystemlog doc

# kdeedu
entry blinken blinken doc
entry kalgebra kalgebra doc
entry kanagram kanagram doc
entry kbruch kbruch doc
entry kgeography kgeography doc
entry khangman khangman doc
entry kiten kiten doc
entry klettres klettres doc
entry kmplot kmplot doc
entry kturtle kturtle doc
entry kwordquiz kwordquiz doc
entry parley parley docs/parley

# kdegames
entry bomber bomber doc
entry bovo bovo doc
entry granatier granatier doc
entry kajongg kajongg doc
entry kapman kapman doc
entry katomic katomic doc
entry kblackbox kblackbox doc
entry kblocks kblocks doc
entry kbounce kbounce doc
entry kbreakout kbreakout doc
entry kdiamond kdiamond doc
entry kfourinline kfourinline doc
entry kgoldrunner kgoldrunner doc
entry kigo kigo doc
entry killbots killbots doc
entry kiriki kiriki doc
entry kjumpingcube kjumpingcube doc
entry klickety klickety doc
entry klines klines doc
entry kmahjongg kmahjongg doc
entry kmines kmines doc
entry knavalbattle knavalbattle doc
entry knetwalk knetwalk doc
entry knights knights doc
entry kolf kolf doc
entry kollision kollision doc
entry konquest konquest doc
entry kpat kpat doc
entry kreversi kreversi doc
entry kshisen kshisen doc
entry ksirk ksirk doc/ksirk
entry ksirkskineditor ksirk doc/ksirkskineditor
entry ksnakeduel ksnakeduel doc
entry kspaceduel kspaceduel doc
entry ksquares ksquares doc
entry ksudoku ksudoku doc
entry ktuberling ktuberling doc
entry kubrick kubrick doc
entry lskat lskat doc
entry palapeli palapeli doc
entry picmi picmi doc

# kdegraphics
entry gwenview gwenview doc
entry kcontrol/kamera kamera doc
entry kimagemapeditor kimagemapeditor doc
entry kolourpaint kolourpaint doc
entry kruler kruler doc
entry okular okular doc
entry spectacle spectacle doc

# kdemultimedia
entry dragonplayer dragon doc
entry kioworker6/audiocd audiocd-kio doc
entry kcontrol/kcmaudiocd audiocd-kio kcmaudiocd/doc
entry k3b k3b doc
entry kdenlive kdenlive doc
entry elisa elisa doc
entry juk juk doc

# pim
entry konsolekalendar akonadi-calendar-tools doc/konsolekalendar
entry importwizard akonadi-import-wizard doc
entry akregator akregator doc
entry contactthemeeditor grantlee-editor doc/contactthemeeditor
entry headerthemeeditor grantlee-editor doc/headerthemeeditor
entry kaddressbook kaddressbook doc
entry kalarm kalarm doc
entry kleopatra kleopatra doc/kleopatra
entry kwatchgnupg kleopatra doc/kwatchgnupg
entry kmail2 kmail doc/kmail2
entry akonadi_archivemail_agent kmail doc/akonadi_archivemail_agent
entry akonadi_followupreminder_agent kmail doc/akonadi_followupreminder_agent
entry akonadi_sendlater_agent kmail doc/akonadi_sendlater_agent
entry ktnef kmail ktnef/doc
entry knotes knotes doc/knotes
entry akonadi_notes_agent knotes doc/akonadi_notes_agent
entry kontact kontact doc/kontact
entry korganizer korganizer doc
entry pimsettingexporter pim-data-exporter doc
entry sieveeditor pim-sieve-editor doc
entry kioworker6/ldap kldap kioworker/doc/ldap

# kdenetwork
entry kcontrol6 kio-extras doc/kcontrol6
entry kdeconnect-kde kdeconnect-kde doc
entry kget kget doc
entry kioworker6 kio-extras doc/kioworker
entry konversation konversation doc
entry krdc krdc doc
entry krfb krfb doc
entry ktorrent ktorrent doc
entry neochat neochat doc

# kdesdk
entry kapptemplate kapptemplate doc
entry kcachegrind kcachegrind doc
entry poxml poxml doc

# kdeutils
entry ark ark doc
entry filelight filelight doc
entry kbackup kbackup doc/en
entry kcalc kcalc doc
entry kcharselect kcharselect doc
entry kcontrol/blockdevices kdf doc/kcontrol
entry kdf kdf doc/app
entry kgpg kgpg doc
entry kteatime kteatime doc
entry ktimer ktimer doc
entry kwalletmanager kwalletmanager doc
entry sweeper sweeper doc

# extragear-edu
entry gcompris gcompris docs/docbook
entry rkward rkward doc/rkward
entry rkwardplugins rkward doc/rkwardplugins

# extragear-games
entry skladnik skladnik doc

# extragear-graphics
entry kgraphviewer kgraphviewer doc/en_US

# extragear-multimedia
entry haruna haruna doc

# extragear-network
entry smb4k smb4k doc

# extragear-office
entry calligra calligra doc/calligra
entry sheets calligra doc/sheets
entry stage calligra doc/stage
entry kile kile doc

# extragear-utils
entry kdiff3 kdiff3 doc/en
entry keurocalc keurocalc doc

# playground-games
entry atlantik atlantik doc

# extragear-pim
entry ktimetracker ktimetracker doc

# playground-utils
entry kregexpeditor kregexpeditor doc

# This is just a placeholder because the script
# is dumb and forgets to process the last line if there is not an empty line at the end
# having this at the end we make sure that is not a problem :-)
