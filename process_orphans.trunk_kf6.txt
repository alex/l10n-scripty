# Rule file for the process_orphans.sh script
# Comments must start with the # character at first column with a space after it
# Empty lines are allowed
# Format: command origin [destination] [revision] [date]
# Available commands: move copy delete merge mergekeep
# Use only *one* space to separate data
# Note: origin and destination must be PO files, not POT files

move docmessages/kpackage/kpackagetool_man-kpackagetool5.1.po docmessages/kpackage/kpackagetool_man-kpackagetool6.1.po
delete docmessages/kservice/desktoptojson_man-desktoptojson.8.po

move messages/kinfocenter/kcmsamba.po messages/kinfocenter/kcm_samba.po
move messages/kgamma5/kgamma5._desktop_.po messages/kgamma5/kgamma5._json_.po
move messages/kcoreaddons/kde5_xml_mimetypes.po messages/kcoreaddons/kde6_xml_mimetypes.po

delete docmessages/kconfigwidgets/preparetips5_man-preparetips5.1.po

delete messages/kcmutils/kcmutils._desktop_.po
delete messages/kpackage/kpackage._desktop_.po
delete messages/plasma-pa/plasma-pa._desktop_.po
delete messages/plasma-vault/plasma-vault._desktop_.po

move messages/plasma-mobile/plasma_applet_org.kde.phone.homescreen.po messages/plasma-mobile/plasma_applet_org.kde.plasma.mobile.homescreen.po
move messages/plasma-mobile/plasma_applet_org.kde.phone.homescreen.simple.po messages/plasma-mobile/plasma_applet_org.kde.plasma.mobile.homescreen.halcyon.po

copy messages/plasma-sdk/plasma-sdk._desktop_.po messages/plasma-sdk/plasma-sdk._json_.po

move messages/karchive/karchive5_qt.po messages/karchive/karchive6_qt.po
move messages/kcodecs/kcodecs5_qt.po messages/kcodecs/kcodecs6_qt.po
move messages/kconfig/kconfig5_qt.po messages/kconfig/kconfig6_qt.po
move messages/kcoreaddons/kcoreaddons5_qt.po messages/kcoreaddons/kcoreaddons6_qt.po
move messages/kdbusaddons/kdbusaddons5_qt.po messages/kdbusaddons/kdbusaddons6_qt.po
move messages/kdnssd/kdnssd5_qt.po messages/kdnssd/kdnssd6_qt.po
move messages/kholidays/libkholidays5_qt.po messages/kholidays/libkholidays6_qt.po
move messages/ki18n/ki18n5.po messages/ki18n/ki18n6.po
move messages/kitemviews/kitemviews5_qt.po messages/kitemviews/kitemviews6_qt.po
move messages/kwidgetsaddons/kwidgetsaddons5_qt.po messages/kwidgetsaddons/kwidgetsaddons6_qt.po
move messages/kwindowsystem/kwindowsystem5_qt.po messages/kwindowsystem/kwindowsystem6_qt.po
move messages/solid/solid5_qt.po messages/solid/solid6_qt.po
move messages/sonnet/sonnet5_qt.po messages/sonnet/sonnet6_qt.po
move messages/syntax-highlighting/syntaxhighlighting5_qt.po messages/syntax-highlighting/syntaxhighlighting6_qt.po

move messages/kauth/kauth5_qt.po messages/kauth/kauth6_qt.po
move messages/kcompletion/kcompletion5_qt.po messages/kcompletion/kcompletion6_qt.po
move messages/kcontacts/kcontacts5.po messages/kcontacts/kcontacts6.po
move messages/knotifications/knotifications5_qt.po messages/knotifications/knotifications6_qt.po
move messages/kfilemetadata/kfilemetadata5.po messages/kfilemetadata/kfilemetadata6.po

move messages/kjobwidgets/kjobwidgets5_qt.po messages/kjobwidgets/kjobwidgets6_qt.po
move messages/kpackage/libkpackage5.po messages/kpackage/libkpackage6.po
move messages/kpeople/kpeople5.po messages/kpeople/kpeople6.po
move messages/kpty/kpty5.po messages/kpty/kpty6.po
move messages/kunitconversion/kunitconversion5.po messages/kunitconversion/kunitconversion6.po
move messages/kcmutils/kcmutils5.po messages/kcmutils/kcmutils6.po
move messages/kbookmarks/kbookmarks5_qt.po messages/kbookmarks/kbookmarks6_qt.po
move messages/kconfigwidgets/kconfigwidgets5.po messages/kconfigwidgets/kconfigwidgets6.po
move messages/kdav/libkdav.po messages/kdav/libkdav6.po
move messages/kglobalaccel/kglobalaccel5_qt.po messages/kglobalaccel/kglobalaccel6_qt.po

move messages/baloo/balooctl5.po messages/baloo/balooctl6.po
move messages/baloo/baloodb5.po messages/baloo/baloodb6.po
move messages/baloo/balooengine5.po messages/baloo/balooengine6.po
move messages/baloo/baloo_file5.po messages/baloo/baloo_file6.po
move messages/baloo/baloo_file_extractor5.po messages/baloo/baloo_file_extractor6.po
move messages/baloo/baloosearch5.po messages/baloo/baloosearch6.po
move messages/baloo/balooshow5.po messages/baloo/balooshow6.po
move messages/baloo/kio5_baloosearch.po messages/baloo/kio6_baloosearch.po
move messages/baloo/kio5_tags.po messages/baloo/kio6_tags.po
move messages/baloo/kio5_timeline.po messages/baloo/kio6_timeline.po
move messages/kdesu/kdesud5.po messages/kdesu/kdesud6.po
move messages/kiconthemes/kiconthemes5.po messages/kiconthemes/kiconthemes6.po
move messages/knewstuff/knewstuff5.po messages/knewstuff/knewstuff6.po
move messages/knotifyconfig/knotifyconfig5.po messages/knotifyconfig/knotifyconfig6.po
move messages/kparts/kparts5.po messages/kparts/kparts6.po
move messages/kservice/kservice5.po messages/kservice/kservice6.po

move messages/kirigami/libkirigami2plugin_qt.po messages/kirigami/libkirigami6_qt.po
move messages/kdeclarative/kdeclarative5.po messages/kdeclarative/kdeclarative6.po
move messages/ktexteditor/ktexteditor5.po messages/ktexteditor/ktexteditor6.po
move messages/ktextwidgets/ktextwidgets5.po messages/ktextwidgets/ktextwidgets6.po
move messages/kwallet/kwalletd5.po messages/kwallet/kwalletd6.po
move messages/kxmlgui/kxmlgui5.po messages/kxmlgui/kxmlgui6.po
move messages/plasma-framework/libplasma5.po messages/plasma-framework/libplasma6.po

move messages/plasma-disks/plasma_disks.po messages/plasma-disks/kcm_disks.po

move messages/purpose/libpurpose_quick.po messages/purpose/libpurpose6_quick.po
move messages/purpose/libpurpose_widgets.po messages/purpose/libpurpose6_widgets.po
move messages/purpose/purpose_barcode.po messages/purpose/purpose6_barcode.po
move messages/purpose/purpose_bluetooth.po messages/purpose/purpose6_bluetooth.po
move messages/purpose/purpose-fileitemaction.po messages/purpose/purpose6_fileitemaction.po
move messages/purpose/purpose_email.po messages/purpose/purpose6_email.po
move messages/purpose/purpose_imgur.po messages/purpose/purpose6_imgur.po
move messages/purpose/purpose_kdeconnect.po messages/purpose/purpose6_kdeconnect.po
move messages/purpose/purpose_kdeconnectsms.po messages/purpose/purpose6_kdeconnectsms.po
move messages/purpose/purpose_ktp-sendfile.po messages/purpose/purpose6_ktp-sendfile.po
move messages/purpose/purpose_nextcloud.po messages/purpose/purpose6_nextcloud.po
move messages/purpose/purpose_pastebin.po messages/purpose/purpose6_pastebin.po
move messages/purpose/purpose_phabricator.po messages/purpose/purpose6_phabricator.po
move messages/purpose/purpose_reviewboard.po messages/purpose/purpose6_reviewboard.po
move messages/purpose/purpose_saveas.po messages/purpose/purpose6_saveas.po
move messages/purpose/purpose_twitter.po messages/purpose/purpose6_twitter.po
move messages/purpose/purpose_youtube.po messages/purpose/purpose6_youtube.po
move messages/kwallet/kwallet-query.po messages/kwallet/kwallet6-query.po

delete docmessages/khotkeys/kcontrol_khotkeys.po
delete messages/khotkeys/khotkeys._desktop_.po
delete messages/khotkeys/khotkeys.po

delete messages/plasma-workspace/plasma_runner_windowedwidgets.po

move messages/plasma-desktop/kcm_activities5.po messages/plasma-desktop/kcm_activities.po

move docmessages/kdoctools/meinproc5_man-meinproc5.1.po docmessages/kdoctools/meinproc6_man-meinproc6.1.po
move docmessages/kdoctools/checkXML5_man-checkXML5.1.po docmessages/kdoctools/checkXML6_man-checkXML6.1.po
move docmessages/kdoctools/kf5options_man-kf5options.7.po docmessages/kdoctools/kf6options_man-kf6options.7.po
move docmessages/kdoctools/qt5options_man-qt5options.7.po docmessages/kdoctools/qt6options_man-qt6options.7.po

move docmessages/kio/kcontrol5_cookies.po docmessages/kio/kcontrol6_cookies.po
move docmessages/kio/kcontrol5_netpref.po docmessages/kio/kcontrol6_netpref.po
move docmessages/kio/kcontrol5_proxy.po docmessages/kio/kcontrol6_proxy.po
move docmessages/kio/kcontrol5_smb.po docmessages/kio/kcontrol6_smb.po
move docmessages/kio/kcontrol5_trash.po docmessages/kio/kcontrol6_trash.po
move docmessages/kio/kcontrol5_webshortcuts.po docmessages/kio/kcontrol6_webshortcuts.po

move docmessages/kservice/kbuildsycoca5_man-kbuildsycoca5.8.po docmessages/kservice/kbuildsycoca6_man-kbuildsycoca6.8.po

# at this point, kio-extras translations were copied from the trunk_kf5 branch
copy messages/kio/kio6.po messages/kio-extras/kio-extras_kcms.po

move docmessages/kio/kcontrol6_cookies.po docmessages/kio-extras/kcontrol6_cookies.po
move docmessages/kio/kcontrol6_netpref.po docmessages/kio-extras/kcontrol6_netpref.po
move docmessages/kio/kcontrol6_proxy.po docmessages/kio-extras/kcontrol6_proxy.po
move docmessages/kio/kcontrol6_smb.po docmessages/kio-extras/kcontrol6_smb.po
move docmessages/kio/kcontrol6_trash.po docmessages/kio-extras/kcontrol6_trash.po
move docmessages/kio/kcontrol6_webshortcuts.po docmessages/kio-extras/kcontrol6_webshortcuts.po

move messages/kio-extras/kfileaudiopreview5.po messages/kio-extras/kfileaudiopreview6.po

move messages/kio-extras/kio5_activities.po messages/kio-extras/kio6_activities.po
move messages/kio-extras/kio5_afc.po messages/kio-extras/kio6_afc.po
move messages/kio-extras/kio5_archive.po messages/kio-extras/kio6_archive.po
move messages/kio-extras/kio5_bookmarks.po messages/kio-extras/kio6_bookmarks.po
move messages/kio-extras/kio5_fish.po messages/kio-extras/kio6_fish.po
move messages/kio-extras/kio5_info.po messages/kio-extras/kio6_info.po
move messages/kio-extras/kio5_man.po messages/kio-extras/kio6_man.po
move messages/kio-extras/kio5_mtp.po messages/kio-extras/kio6_mtp.po
move messages/kio-extras/kio5_nfs.po messages/kio-extras/kio6_nfs.po
move messages/kio-extras/kio5_recentdocuments.po messages/kio-extras/kio6_recentdocuments.po
move messages/kio-extras/kio5_sftp.po messages/kio-extras/kio6_sftp.po
move messages/kio-extras/kio5_smb.po messages/kio-extras/kio6_smb.po
move messages/kio-extras/kio5_thumbnail.po messages/kio-extras/kio6_thumbnail.po

move docmessages/kio-extras/kioslave5_bookmarks.po docmessages/kio-extras/kioworker6_bookmarks.po
move docmessages/kio-extras/kioslave5_bzip2.po docmessages/kio-extras/kioworker6_bzip2.po
move docmessages/kio-extras/kioslave5_fish.po docmessages/kio-extras/kioworker6_fish.po
move docmessages/kio-extras/kioslave5_gzip.po docmessages/kio-extras/kioworker6_gzip.po
move docmessages/kio-extras/kioslave5_info.po docmessages/kio-extras/kioworker6_info.po
move docmessages/kio-extras/kioslave5_man.po docmessages/kio-extras/kioworker6_man.po
move docmessages/kio-extras/kioslave5_nfs.po docmessages/kio-extras/kioworker6_nfs.po
move docmessages/kio-extras/kioslave5_recentdocuments.po docmessages/kio-extras/kioworker6_recentdocuments.po
move docmessages/kio-extras/kioslave5_sftp.po docmessages/kio-extras/kioworker6_sftp.po
move docmessages/kio-extras/kioslave5_smb.po docmessages/kio-extras/kioworker6_smb.po
move docmessages/kio-extras/kioslave5_tar.po docmessages/kio-extras/kioworker6_tar.po
move docmessages/kio-extras/kioslave5_thumbnail.po docmessages/kio-extras/kioworker6_thumbnail.po
move docmessages/kio-extras/kioslave5_xz.po docmessages/kio-extras/kioworker6_xz.po
move docmessages/kio-extras/kioslave5_zstd.po docmessages/kio-extras/kioworker6_zstd.po

copy messages/plasma-nano/plasma-nano._desktop_.po messages/plasma-nano/plasma-nano._json_.po

delete messages/kde-cli-tools/ktraderclient5.po

delete messages/kio-extras/kio6_recentdocuments.po
delete docmessages/kio-extras/kioworker6_recentdocuments.po

move docmessages/kded/kded5_man-kded5.8.po docmessages/kded/kded6_man-kded6.8.po

move messages/ksvg/libksvg5.po messages/ksvg/libksvg6.po
move messages/libkscreen/libkscreen5_qt.po messages/libkscreen/libkscreen6_qt.po

move docmessages/kde-cli-tools/kcontrol5_filetypes.po docmessages/kde-cli-tools/kcontrol6_filetypes.po

move messages/kactivitymanagerd/kactivities5.po messages/kactivitymanagerd/kactivities6.po

move messages/libkdegames/libkdegames5.po messages/libkdegames/libkdegames6.po
move messages/libkmahjongg/libkmahjongg5.po messages/libkmahjongg/libkmahjongg6.po

move messages/powerdevil/powerdevilactivitiesconfig.po messages/powerdevil/kcm_powerdevilglobalconfig.po
move messages/powerdevil/kcm_powerdevilglobalconfig.po messages/powerdevil/kcm_powerdevilactivityconfig.po
move messages/powerdevil/powerdevilglobalconfig.po messages/powerdevil/kcm_powerdevilglobalconfig.po
move messages/powerdevil/powerdevilprofilesconfig.po messages/powerdevil/kcm_powerdevilprofilesconfig.po
move messages/powerdevil/kcm_powerdevilactivityconfig.po messages/powerdevil/kcm_powerdevilactivitiesconfig.po

move messages/oxygen-icons/oxygen-icons5._desktop_.po messages/oxygen-icons/oxygen-icons._desktop_.po

move messages/svgpart/svgpart._desktop_.po messages/svgpart/svgpart._json_.po

delete messages/plasma-nano/plasma-nano._desktop_.po

copy messages/kclock/kclockd.po messages/kclock/kclock.po

move messages/kuserfeedback/userfeedbackconsole5_qt.po messages/kuserfeedback/userfeedbackconsole6_qt.po
move messages/kuserfeedback/userfeedbackprovider5_qt.po messages/kuserfeedback/userfeedbackprovider6_qt.po

move messages/kweathercore/kweathercore5.po messages/kweathercore/kweathercore6.po
move messages/kdiagram/kchart_qt.po messages/kdiagram/kchart6_qt.po
move messages/kdiagram/kgantt_qt.po messages/kdiagram/kgantt6_qt.po

move messages/kldap/libkldap5.po messages/kldap/libkldap6.po
move docmessages/kldap/kioslave5_ldap.po docmessages/kldap/kioworker6_ldap.po
move messages/kimap/libkimap5.po messages/kimap/libkimap6.po
move messages/kmime/libkmime5.po messages/kmime/libkmime6.po
move messages/kcalutils/libkcalutils5.po messages/kcalutils/libkcalutils6.po
move messages/ktnef/libktnef5.po messages/ktnef/libktnef6.po
move messages/kidentitymanagement/libkpimidentities5.po messages/kidentitymanagement/libkpimidentities6.po
move messages/kmailtransport/libmailtransport5.po messages/kmailtransport/libmailtransport6.po
move messages/kontactinterface/kontactinterfaces5.po messages/kontactinterface/kontactinterfaces6.po
move messages/ksmtp/libksmtp5.po messages/ksmtp/libksmtp6.po
move messages/akonadi/libakonadi5.po messages/akonadi/libakonadi6.po
move messages/akonadi-calendar/libakonadi-calendar5.po messages/akonadi-calendar/libakonadi-calendar6.po
move messages/akonadi-calendar/libakonadi-calendar5-serializer.po messages/akonadi-calendar/libakonadi-calendar6-serializer.po
move messages/akonadi-contacts/akonadicontact5.po messages/akonadi-contacts/akonadicontact6.po
move messages/akonadi-contacts/akonadicontact5-serializer.po messages/akonadi-contacts/akonadicontact6-serializer.po
move messages/akonadi-mime/libakonadi-kmime5.po messages/akonadi-mime/libakonadi-kmime6.po
move messages/akonadi-notes/akonadinotes5.po messages/akonadi-notes/akonadinotes6.po

move messages/calendarsupport/calendarsupport.po messages/calendarsupport/calendarsupport6.po
move messages/eventviews/libeventviews.po messages/eventviews/libeventviews6.po
move messages/incidenceeditor/libincidenceeditors.po messages/incidenceeditor/libincidenceeditors6.po
move messages/kitinerary/kitinerary.po messages/kitinerary/kitinerary6.po
move messages/kpimtextedit/libkpimtextedit.po messages/kpimtextedit/libkpimtextedit6.po
move messages/libkdepim/libkdepim.po messages/libkdepim/libkdepim6.po
move messages/libkgapi/libkgapi_qt.po messages/libkgapi/libkgapi6_qt.po
move messages/libkleo/libkleopatra.po messages/libkleo/libkleopatra6.po
move messages/libksieve/libksieve.po messages/libksieve/libksieve6.po
move messages/mailimporter/libmailimporter.po messages/mailimporter/libmailimporter6.po
move messages/messagelib/libmessagecomposer.po messages/messagelib/libmessagecomposer6.po
move messages/messagelib/libmessagecore.po messages/messagelib/libmessagecore6.po
move messages/messagelib/libmessagelist.po messages/messagelib/libmessagelist6.po
move messages/messagelib/libmessageviewer.po messages/messagelib/libmessageviewer6.po
move messages/messagelib/libmimetreeparser.po messages/messagelib/libmimetreeparser6.po
move messages/messagelib/libtemplateparser.po messages/messagelib/libtemplateparser6.po
move messages/messagelib/libwebengineviewer.po messages/messagelib/libwebengineviewer6.po

move messages/grantleetheme/libgrantleetheme.po messages/grantleetheme/libgrantleetheme6.po
move messages/libgravatar/libgravatar.po messages/libgravatar/libgravatar6.po
move messages/mailcommon/libmailcommon.po messages/mailcommon/libmailcommon6.po
move messages/mimetreeparser/mimetreeparser.po messages/mimetreeparser/mimetreeparser6.po
move messages/pimcommon/libpimcommon.po messages/pimcommon/libpimcommon6.po

move messages/khelpcenter/khelpcenter5.po messages/khelpcenter/khelpcenter6.po

move docmessages/audiocd-kio/kioslave5_audiocd.po docmessages/audiocd-kio/kioworker6_audiocd.po

delete docmessages/kio-extras/kioworker6_nfs.po

move messages/plasma-mobile/kcm_mobile_powermanagement.po messages/plasma-mobile/kcm_mobile_power.po

move messages/kdesdk-kio/kio5_perldoc.po messages/kdesdk-kio/kio6_perldoc.po

move docmessages/kwalletmanager/kwallet5.po docmessages/kwalletmanager/kwalletmanager.po

move messages/kmag/org.kde.kmag.appdata.po messages/kmag/org.kde.kmag.metainfo.po
